/**
 * <pre>
 * 1. 개요 : 공통 처리
 * 2. 처리내용 : 공통사항을 처리하기 위한 스크립트
 * </pre>
 * 
 * 
 * @File Name businessManagement.js
 * @Author : creme55
 * @Date : 2016. 10. 18.
 * @Version : 1.0
 *
 * 
 */

function bManagementLeftMenuAct(code){
		if(code==0){
			location.href = "/businessManagement/intermediaryBusiness.do";			
		}else if(code==1){
			location.href = "/businessManagement/resourceholders.do";
		}
		
}
fnMap.bM.updateintermediaryBusiness =  function(){
	$.ajax({
		url : '/businessManagement/intermediaryBusinessModCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			if(data.status==0)
				location.href = "/businessManagement/intermediaryBusiness.do";
			else if(data.status==1){
				var dataArr=data.params.split(",");
				var msg="필수 입력사항이 누락되었습니다.</br></br>";
				for ( var data in dataArr) {
					msg+=dataArr[data]+"</br>";
				}
				$.jQueryMsgAlert(msg);
			}
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});
};
fnMap.bM.deleteintermediaryBusiness =  function(){
	$.ajax({
		url : '/businessManagement/intermediaryBusinessDelCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			location.href = "/businessManagement/intermediaryBusiness.do";
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});
};
fnMap.bM.addintermediaryBusiness = function(){
	$.ajax({
		url : '/businessManagement/intermediaryBusinessAddCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			if(data.status==0)
				location.href = "/businessManagement/intermediaryBusiness.do";
			else if(data.status==1){
				var dataArr=data.params.split(",");
				var msg="필수 입력사항이 누락되었습니다.</br></br>";
				for ( var data in dataArr) {
					msg+=dataArr[data]+"</br>";
				}
				$.jQueryMsgAlert(msg);
			}
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});
};
fnMap.bM.updateResource = function(){
	$.ajax({
		url : '/businessManagement/resourceModCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			if(data.status==0)
				location.href = "/businessManagement/resourceholders.do";	
			else if(data.status==1){
				var dataArr=data.params.split(",");
				var msg="필수 입력사항이 누락되었습니다.</br></br>";
				for ( var data in dataArr) {
					msg+=dataArr[data]+"</br>";
				}
				$.jQueryMsgAlert(msg);
			}
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});	
};
fnMap.bM.deleteResource = function(){
	$.ajax({
		url : '/businessManagement/resourceDelCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			location.href = "/businessManagement/resourceholders.do";
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});	
};
fnMap.bM.addResource =function(){
	$.ajax({
		url : '/businessManagement/resourceAddCmd.do'
		,type : 'POST'
		,data : $("#bm_popup").serialize()
		,success : function(data) {
			if(data.status==0)
				location.href = "/businessManagement/resourceholders.do";	
			else if(data.status==1){
				var dataArr=data.params.split(",");
				var msg="필수 입력사항이 누락되었습니다.</br></br>";
				for ( var data in dataArr) {
					msg+=dataArr[data]+"</br>";
				}
				$.jQueryMsgAlert(msg);
			}
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});	
}; 
fnMap.bM.addPopUp = function(title,code){
	$('#popContext').load(fnMap.bM.getUri(code),{"code":"add"},function(){
		popUpSet('popUpDialog', 'open',title+' 등록');
		var fnNumber=function(){
			chkTxtCnt(this,4);
		};
		for(var i=1;i<4;i++){
			$("input[name=cnct_no"+i+"]").keyup(fnNumber);
			$("input[name=tel_no"+i+"]").keyup(fnNumber);
			$("input[name=fax_no"+i+"]").keyup(fnNumber);
		}
	});
};
fnMap.bM.modifyPopUp = function(title,code,obj){
	$('#popContext').load(fnMap.bM.getUri(code),{"code":"modify","id":obj},function(){
		popUpSet('popUpDialog', 'open',title+' 수정');
		var fnNumber=function(){
			chkTxtCnt(this,4);
		};
		for(var i=1;i<4;i++){
			$("input[name=cnct_no"+i+"]").keyup(fnNumber);
			$("input[name=tel_no"+i+"]").keyup(fnNumber);
			$("input[name=fax_no"+i+"]").keyup(fnNumber);
		}
	});
};
fnMap.bM.getUri = function(code){
	var result="";
	switch (code) {
	case 0:
		result='./intermediaryBusinessAddNModify.do';
		break;

	case 1:
		result='./resourceAddNModify.do';		
		break;

	default:
		break;
	}
	return result;
};

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div style="width: 100%;overflow: auto;">
	<div style="float: right;">
		<span style="font-weight: bold;">중개사업자명</span>
		<select>
			<option>중개사업자명</option>
			<option>1</option>
			<option>2</option>
		</select>
		<span style="font-weight: bold;">판매희망일</span>
		<input type="text" name="shDate" id="shDate"/>
	</div>
</div>
<div style="width: 100%">
	<table id="popList"></table>
	<div id="popListPager"></div>
</div> 
<c:if test="${param.code eq 'add'}">
	<div class="form-group" style="float: right; width: 290px;">
		<a class="btn_big4" style="float: right;">
			제출
		</a>
	</div>
</c:if>
<c:if test="${param.code eq 'modify'}">
	<div class="form-group" style="float: right;width: 290px;">
		<a class="btn_big4" style="float: right;">
			수정
		</a>
	</div>
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
/**
 *  ETRI Distributed Resource/Mediation System for new re-generation Energy Exchange
 *
 *  Copyright ⓒ [2016] ETRI. All rights reserved.
 *
 *    This is a proprietary software of ETRI, and you may not use this file except in
 *  compliance with license agreement with ETRI. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of ETRI, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 *
 * @author creme55
 * @since 2016. 10. 17.
 * @version 1.0
 * @see 
 * @Copyright ? [2016] By ETRI. All rights reserved.
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *      수정일                                       수정자                                                  수정내용
 *  -------------        -----------       -------------------------
 *  2016. 10. 17.          creme55         최초 생성 (메인페이지, 홈페이지 이동)
 *
 * </pre>
 **/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> </title>
</head>
<body>
<div style="width: 80%;float: right;margin: 10px;">
	<form id="calc_form">
		<table class="tb2" style="width: 100%;">
			<colgroup>
				<col style="width: 25%;"/>
				<col style="width: 25%;"/>
				<col style="width: 25%;"/>
				<col style="width: 25%;"/>
			</colgroup>
			<tr>
				<th colspan="4">정산</th>
			</tr>
			<c:forEach begin="0" end="${fn:length(calcBas)}" varStatus="status" var="num" step="2">
				<tr>
					<th>${calcBas[num].calc_nm}</th>
					<td>
						<input type="text" name="${calcBas[num].seq_no }" value="${calcBas[num].calc_val}"/>
					</td>
					<th>${calcBas[num+1].calc_nm}</th>
					<td>
						<c:if test="${calcBas[num+1] != null }">
							<input type="text" name="${calcBas[num+1].seq_no }" value="${calcBas[num+1].calc_val}"/>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
		<div class="form-group" style="float: right;margin-right : 0px;">
			<a class="btn_big4" style="float: right;margin-right : 0px;"
				 href="javascript:fnMap.set.updateCalc()">
				수정
			</a>
		</div>
	</form>
	<div style="height: 20px;">&nbsp;</div>
	<table id="planData" class="tb2" style="width: 100%;">
		<colgroup>
			<col style="width: 40%;" />
			<col style="width: 50%;" />
			<col style="width: 10%;" />
		</colgroup>
		<thead>
			<tr>
				<th colspan="3">스케줄시간</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${results}" varStatus="status">
				<tr style="line-height: 30px;">
					<c:set var="timeInv" value="${fn:split(item.time_interval,' ')}"/>
					<th>
						<c:out value="${item.context_name }"></c:out>
						<input type="hidden" value="${item.order_seq }"/>
					</th>
					<td>
						<select name="day">
							<c:forEach begin="0" end="4" varStatus="status" var="num">
	   							<c:set var="val" value="${2-num}"/>
	   							<c:if test="${item.day_interval == val }">
									<option selected="selected" value="${val }">D${val < 0 ? '-':(val eq 0 ? '&nbsp;':'+')}${val < 0 ? -val:val }</option>
								</c:if>
	   							<c:if test="${item.day_interval != val }">
									<option value="${val }">D${val < 0 ? '-':(val eq 0 ? '&nbsp;':'+')}${val < 0 ? -val:val }</option>
								</c:if>
							</c:forEach>
						</select>
						일
						<select name="HH">
							<c:if test="${timeInv[2] eq '*'}">
								<option selected="selected" value="*">매 시</option>
							</c:if>
							<c:if test="${timeInv[2] ne '*'}">
								<option  value="*">매 시</option>
							</c:if>
							<c:forEach begin="0" end="23" varStatus="status" var="num">
								<fmt:formatNumber var="no" minIntegerDigits="2" value="${num}"/>
								<c:if test="${timeInv[2] eq no}">
									<option selected="selected"  value="${no }">${no }</option>
								</c:if>
								<c:if test="${timeInv[2] ne no}">
									<option value="${no }">${no }</option>
								</c:if>
							</c:forEach>
						</select>
						시
						<select name="MI">
							<c:if test="${timeInv[1] eq '*'}">
								<option selected="selected" value="*">매 분</option>
							</c:if>
							<c:if test="${timeInv[1] ne '*'}">
								<option value="*">매 분</option>
							</c:if>
							<c:forEach begin="0" end="59" varStatus="status" var="num">
								<fmt:formatNumber var="no" minIntegerDigits="2" value="${num}"/>
								<c:if test="${timeInv[1] eq no}">
									<option selected="selected" value="${no }">${no }</option>
								</c:if>
								<c:if test="${timeInv[1] ne no}">
									<option value="${no }">${no }</option>
								</c:if>
							</c:forEach>
						</select>
						분
					</td>
					<td>
						<c:if test="${item.use_yn == 'y' or item.use_yn == 'Y' }">
							<label for="chk${status.num }"><input type="checkbox" checked="checked" id="chk${status.num }" name="chk${status.num }" style="margin-top: 0px;vertical-align:middle;"/>사용여부</label>
						</c:if>
						<c:if test="${item.use_yn == 'n' or item.use_yn == 'N' }">
							<label for="chk${status.num }"><input type="checkbox" id="chk${status.num }" name="chk${status.num }" style="margin-top: 0px;vertical-align:middle;"/>사용여부</label>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="form-group" style="float: right;margin-right : 0px;">
		<a class="btn_big4" style="float: right;margin-right : 0px;"
			 href="javascript:fnMap.set.updatePlan()">
			수정
		</a>
	</div>
</div>
<script>
	$('#left_menu_1').attr('class','active');
</script>
</body>
</html>
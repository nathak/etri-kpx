<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<table class="tb2" style="width: 100%;height: 100%;float: right;">
	<colgroup>
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
		<col style="width:12%;">
	</colgroup>
	<thead>
		<tr>
			<th>시간</th>
			<th>${results[0].title}</th>
			<th>${results[1].title}</th>
			<th>${results[2].title}</th>
			<th>${results[3].title}</th>
			<th>${results[4].title}</th>
			<th>${results[5].title}</th>
			<th>${results[6].title}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach begin="1" end="24" varStatus="status">
			<fmt:formatNumber var="no" minIntegerDigits="2" value="${status.count}"/>
			<c:set value="hr_${no}" var="valKey"/>
			<tr>
				<td><c:out value="HR_${no}"/></td>
				<td><c:out value="${results[0][valKey]}"/></td>
				<td><c:out value="${results[1][valKey]}"/></td>
				<td><c:out value="${results[2][valKey]}"/></td>
				<td><c:out value="${results[3][valKey]}"/></td>
				<td><c:out value="${results[4][valKey]}"/></td>
				<td><c:out value="${results[5][valKey]}"/></td>
				<td><c:out value="${results[6][valKey]}"/></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td>평균</td>
			<td><fmt:formatNumber pattern=".00" value="${results[0].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[1].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[2].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[3].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[4].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[5].hr_avg}"/></td>
			<td><fmt:formatNumber pattern=".00" value="${results[6].hr_avg}"/></td>
		</tr>
	</tfoot>
</table>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<script type="text/javascript">
	$(function () {
		var gridData = ${results};
		rowSetting = function (rowId, val, rawObject, cm) {
		    var attr = rawObject.attr[cm.name], result;
		    if (attr.rowspan) {
			    result = ' rowspan=' + '"' + attr.rowspan + '"';
		    } else if (attr.display) {
			    result = ' style="display:' + attr.display + '"';
		    }
		    return result;
		};
		colSetting = function (rowId, val, rawObject, cm) {
		    if(val == '합계')	return ' colspan=3,style="text-align: center;"';
		    if(rawObject.enpr_nm == '합계'  && val !='합계')	
		    	return ' style="display:none;"';
		};
		var colNameArr = ["거래일짜","집합발전기명","SMP","수수료","계량전력량합","정산금","최종정산금"];
		for(var num=0;num<24;num++){
			if(num+1<10)
				colNameArr.push("0"+(num+1)+'구간');
			else
				colNameArr.push((num+1)+'구간');
		}
		var colModelArr = [];
		colModelArr.push({ name: "trx_dt",index:"trx_dt", width:80, align:'center', frozen: true, cellattr: rowSetting });
		colModelArr.push({ name: "enpr_nm",index:"enpr_nm", width:90, align:'center', frozen: true,cellattr:colSetting});
		colModelArr.push({ name: "smp_val",index:"smp_val", width:60, align:'center', frozen: true,cellattr:colSetting});
		colModelArr.push({ name: 'tot_fee',index:"tot_fee", width:60, align:'center', frozen: true,cellattr:colSetting});
		colModelArr.push({ name: "tot_msrmt_sum",index:"tot_msrmt_sum", width:80, align:'center', frozen: true});
		colModelArr.push({ name: "tot_cls_amt",index:"tot_cls_amt", width:60, align:'center', frozen: true});
		colModelArr.push({ name: 'lst_cls_amt',index:"lst_cls_amt", width:60, align:'center', frozen: true});
		for(var num=0;num<24;num++){
			if(num+1<10)
				colModelArr.push({ name: "hr_0"+(num+1),index:'hr_0'+(num+1),width:50});
			else
				colModelArr.push({ name: "hr_"+(num+1),index:'hr_'+(num+1),width:50});
		}
		setJqGridFormKPX('list', colNameArr, colModelArr, '',1000,'auto',gridData);
		$('#left_menu_0').attr('class','active');
		setDatePickerOne('shDate');
	});
	function callOpenAPI(){
		$.ajax({
			url : '/cli/putPwrAdjtInfoRgst.json'
			,type : 'POST'
			,dataType: "json"
			,data : {
				startTime : $('#shDate').val()
			}
			,success : function(data) {
				alert(data.status);
			}
			,fail : function(request, status, error) {
				
			}
		});
	}
	</script>
	<form id="fm_Param" class="form-group" style="float: right;">
		<table class="tb5" style="float: right;width: 480px;display: block;">
			<colgroup>
				<col style="width:130px;">
				<col style="width:130px;">
				<col style="width:130px;">
				<col style="width:85px;">
			</colgroup>
			<tr>
				<td>
					<select name="mdt_id" style="width: 100%;">
						<option value="ALL">모든 중개사업자</option>
						<c:forEach var="item" items="${mdts}">
							<option value="${item.id }">${item.nm}</option>
						</c:forEach>
					</select>
				</td>		
				<td>
					<select name="mdt_rs_id" style="width: 100%;">
						<option value="ALL">모든 집합발전기</option>
						<c:forEach var="item" items="${mdtRs}">
							<option value="${item.id }">${item.nm}</option>
						</c:forEach>
					</select>
				</td>
				<td>
					<input type="text" name="shDate" id="shDate" style="width: 80px;"/>
				</td>
				<td>
					<a class="btn_big" style="float: right;"  href="javascript:shCalculate('1');">
						<img src="../images/img/ico_magnifier.png"/>
						검색
					</a>
				</td>
				<td>
					<a class="btn_big2" style="float: right;" href="javascript:callOpenAPI()">
						<img src="../images/img/ico_add.png"/>
						연계
					</a>
				</td>
			</tr>			
		</table>
	</form>
	
	<div style="float: right;">
		<table id="list"></table>
	</div>
</body>
</html>
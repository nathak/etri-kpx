<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> </title>
<script>
	function callOpenAPI(){
		$.ajax({
			url : '/cli/getEnprInfoList.json'
			,type : 'POST'
			,data : {
				enprTp : 2
			}
			,success : function(data) {
				window.location.reload(true);
			}
			,fail : function(request, status, error) {
				window.location.reload(true);
			}
		});
	}
</script>
</head>
<body>
<div class="form-group" style="float: right;width: 290px;">
	<a class="btn_big2" style="float: right;" href="javascript:fnMap.bM.addPopUp('중개사업자',0)">
		<img src="../images/img/ico_add.png"/>
		추가
	</a>
	<a class="btn_big2" style="float: right;" href="javascript:callOpenAPI()">
		<img src="../images/img/ico_add.png"/>
		연계
	</a>
</div>

<table class="tb2" style="width: 80%;height: 100%;float: right;">
	<thead>
		<tr>
			<th>번호</th>
			<th>중개사업자명</th>
			<th>총위착발전기대수</th>
			<th>위탁관리발전총용량</th>
			<th>등록일</th>
			<th>처리</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="result" items="${results}" varStatus="status">
			<tr>
				<td><c:out value="${status.count}"/></td>
				<td><c:out value="${result.enpr_nm}"/></td>
				<td><c:out value="${result.gnr_cnt}"/></td>
				<td><c:out value="${result.tot_eqpm_cpct}"/></td>
				<td><c:out value="${result.crt_dt}"/></td>
				<td>
					<a class="btn_big" href="javascript:fnMap.bM.modifyPopUp('중개사업자',0,'${result.enpr_id}')">
						<img src="../images/img/ico_modify.png"/>
						변경
					</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<div id="popUpDialog" style="display:none;" >
	<div id="popContext"/>
</div>
<script>
	$('#left_menu_0').attr('class','active');
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:set var="cnct_no" value="${fn:split(results[0].enpr_cnct_cp,'-')}"/>
<c:set var="tel_no" value="${fn:split(results[0].enpr_tel_no,'-')}"/>
<c:set var="fax_no" value="${fn:split(results[0].enpr_fax_no,'-')}"/>
<form id="bm_popup" style="width: 100%;min-width: 800px;">
	<table class="tb4" style="width: 100%;">
		<tr>
			<th><font color="red">*</font>자원보유자사업자명</th>
			<td><input name="enpr_nm" type="text" value="${results[0].enpr_nm}"/></td>
			<th><font color="red">*</font>대표자명</th>
			<td><input name="enpr_ceo_nm" type="text" value="${results[0].enpr_ceo_nm}"/></td>
		</tr>
		<tr>
			<th><font color="red">*</font>사업자 등록번호</th>
			<td><input name="enpr_rgst_no" type="text" value="${results[0].enpr_rgst_no}"/></td>
			<th>자원보유자 홈페이지</th>
			<td><input name="enpr_id" type="text" value="${results[0].enpr_web_page}"/></td>
		</tr>
		<tr>
			<th><font color="red">*</font>업태(업종)</th>
			<td><input name="enpr_cond" type="text" value="${results[0].enpr_cond}"/></td>
			<th>사업자 이메일</th>
			<td><input name="enpr_email" type="text" value="${results[0].enpr_email}"/></td>
		</tr>
		<tr>
			<th><font color="red">*</font>담당자명</th>
			<td><input name="enpr_cnct_nm" type="text" value="${results[0].enpr_cnct_nm}"/></td>
			<th><font color="red">*</font>담당자 전화번호</th>
			<td>
				<input name="cnct_no1" type="text"  style="width:33px;" value="${cnct_no[0]}"/>-
				<input name="cnct_no2" type="text"  style="width:33px;" value="${cnct_no[1]}"/>-
				<input name="cnct_no3" type="text"  style="width:33px;" value="${cnct_no[2]}"/>
			</td>
		</tr>
		<tr>
			<th>회사전화번호</th>
			<td>
				<input name="tel_no1" type="text"  style="width:33px;" value="${tel_no[0]}"/>-
				<input name="tel_no2" type="text"  style="width:33px;" value="${tel_no[1]}"/>-
				<input name="tel_no3" type="text"  style="width:33px;" value="${tel_no[2]}"/>
			</td>
			<th>FAX</th>
			<td>
				<input name="fax_no1" type="text"  style="width:33px;" value="${fax_no[0]}"/>-
				<input name="fax_no2" type="text"  style="width:33px;" value="${fax_no[1]}"/>-
				<input name="fax_no3" type="text"  style="width:33px;" value="${fax_no[2]}"/>
			</td>
		</tr>
		<tr>
			<th rowspan="2"><font color="red">*</font>사업장소재지(기본)</th>
			<td colspan="3">
				<span style="vertical-align: middle;">우편번호</span>
				<input name="enpr_addr_number" type="text" value="${fn:substring(results[0].enpr_addr,0,fn:indexOf(results[0].enpr_addr,' '))}"/>
<!-- 				<a class="btn_sm1" style="float: left;" href="javascript:zipPopOpen('zipCode','zipDialog');"> -->
<!-- 					<img src="../images/img/ico_magnifier_sm.png"/> -->
<!-- 					우편번호 -->
<!-- 				</a> -->
			</td>
<!-- 			<td colspan="3"> -->
<%-- 				<input name="enpr_addr_number" type="text" style="float: left;" value="${fn:substring(results[0].enpr_addr,0,fn:indexOf(results[0].enpr_addr,' '))}"/> --%>
<!-- 				<a class="btn_sm1" style="float: left;" href="javascript:zipPopOpen('zipCode','zipDialog');"> -->
<!-- 					<img src="../images/img/ico_magnifier_sm.png"/> -->
<!-- 					우편번호 -->
<!-- 				</a> -->
<!-- 			</td> -->
		</tr>
		<tr>
			<td colspan="3"><input name="enpr_addr" type="text" style="width:550px;" value="${fn:substring(results[0].enpr_addr,fn:indexOf(results[0].enpr_addr,' '),fn:length(results[0].enpr_addr))}"/></td>
		</tr>
		<tr>
			<th><font color="red">*</font>사업장소재지(상세)</th>
			<td colspan="3"><input name="enpr_dtl_addr" type="text" style="width:550px;" value="${results[0].enpr_dtl_addr}"/></td>
		</tr>
	</table>
	<c:if test="${param.code eq 'add'}">
		<div class="form-group" style="float: right;width: 290px;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.bM.addResource()">
				등록
			</a>
		</div>
	</c:if>
	<c:if test="${param.code eq 'modify'}">
		<input name="seq_no" type="hidden" value="${results[0].seq_no }">
		<div class="form-group" style="float: right;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.bM.updateResource()">
<!-- 			<a class="btn_big4" style="float: right;" href="javascript:fnMap.bM.addResource()"> -->
				수정
			</a>
		</div>
		<div class="form-group" style="float: right;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.bM.deleteResource()">
				삭제
			</a>
		</div>
	</c:if>
</form>
<div id="zipDialog" style="display:none;" >
	<div id="zipCode"/>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
var number=0;
var result=${resourceAllList};
var useds=${usedsJson};
var pwrGnrs=${pwrGnrsJson};
function rs_del(){
	$("#dataTable input:checkbox").each( function() {
			if($(this).is(":checked")){
				if($(this).attr("id")!="allChk")
					$(this).parent().parent().remove();
			}			
		});	
	var totalKW=0;
		var totalCnt=0;
		$("#dataTable tr").each( function() {
			if($(this).find("td").eq(5).text()!=null && $(this).find("td").eq(5).text()!=""){
				totalKW+=Number($(this).find("td").eq(5).text());
				totalCnt++;
			}
		});
		$("#tot_eqpm_cpct").html(totalKW);
		$("#gnr_cnt").html(totalCnt);
}
function rs_add(){
	$("#dataTable").append(
			"<tr>																		"
			+ " <td><input id='chkRsNm"+number+"' type='checkbox'/> </td>										"
			+ "	<td>																	"
			+ "		<select id='selectRsNm"+number+"' onchange='javascript:selectVal("+number+")'>									"
			+ "		</select>															"
			+ "	</td>																	"
			+ "	<td></td>																"
			+ "	<td></td>																"
			+ "	<td></td>																"
			+ "	<td></td>																"
			+ "	<td></td>																"
			+ "</tr>																	"
			);	
	var temp;
	var i=0;
	for ( var obj in result) {
		temp+="<option value='"+result[i].rsrs_id+"' >"+result[i].rsrs_nm+"</option>";
		i++;
	}
	$("#selectRsNm"+number).append("<option value='' >소규모 자원명</option>"+temp);
	
	number++;
}
function selectVal(param){
	$.ajax({
		url : "/resourceManagement/getRessource.json"
		,type : 'POST'
		,dataType : 'json'
		,data : {"rsrs_id":$('#selectRsNm'+param+' option:selected').val(),"regn":$('#regn option:selected').val()}
		,success : function(data) {
			var obj =  JSON.parse(data.results)[0];
			if(obj==null){
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(2).html("");
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(3).html("");
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(4).html("");
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(5).html("");
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(6).html("");
			}else{
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(2).html(obj.enpr_nm);
	 			for ( var num in pwrGnrs) {
	 				if(pwrGnrs[num].code_id==obj.pwr_gnr){
	 					$("#selectRsNm"+param).parent().parent().find('td').eq(3).html(pwrGnrs[num].code_nm);
// 	 		 			$("#selectRsNm"+param).parent().parent().find('td').eq(3).html(obj.pwr_gnr);
	 				}
				}
	 			for ( var num in useds) {
	 				if(useds[num].code_id==obj.used){
	 					$("#selectRsNm"+param).parent().parent().find('td').eq(4).html(useds[num].code_nm);
// 	 		 			$("#selectRsNm"+param).parent().parent().find('td').eq(4).html(obj.used);
	 				}
				}
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(5).html(obj.eqpm_cpct);
	 			$("#selectRsNm"+param).parent().parent().find('td').eq(6).html(obj.crt_dt);		
			}
 			var totalKW=0;
 			var totalCnt=0;
 			$("#dataTable tr").each( function() {
 				if($(this).find("td").eq(5).text()!=null && $(this).find("td").eq(5).text()!=""){
 					totalKW+=Number($(this).find("td").eq(5).text());
 					totalCnt++;
 				}
 			});
 			$("#tot_eqpm_cpct").html(totalKW);
 			$("#gnr_cnt").html(totalCnt);
 			
		}
		,fail : function(request, status, error) {
			$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
			loading_end();
		}
	});
}
$(function () {
	$("#allChk").click(function(){
		if($("#allChk").is(":checked")){
			$("#dataTable input:checkbox").each( function() {
				this.checked=true;
			});
		}else{
			$("#dataTable input:checkbox").each( function() {
				this.checked=false;
			});
			
		}
	});
	$("#regn").change(function(){
		$.ajax({
			url : "/resourceManagement/getRessource.json"
			,type : 'POST'
			,dataType : 'json'
			,data : {"regn":$('#regn option:selected').val()}
			,success : function(data) {
				result =  JSON.parse(data.results);
				for(var i=0;i<=number;i++){
					$("#selectRsNm"+i).parent().parent().remove();
				}
				number=0;
			}
			,fail : function(request, status, error) {
				$.jQueryMsgAlert("code : [" + request.status + "]\n / " + "message : [" + request.responseText + "]\n / " + "error : " + error + "]");
				loading_end();
			}
		});
	});
	number=0;
	
	var totalKW=0;
	var totalCnt=0;
	$("#dataTable tr").each( function() {
		if($(this).find("td").eq(5).text()!=null && $(this).find("td").eq(5).text()!=""){
			totalKW+=Number($(this).find("td").eq(5).text());
			totalCnt++;
		}
	});
	$("#tot_eqpm_cpct").html(totalKW);
	$("#gnr_cnt").html(totalCnt);
});
</script>
<div style="width: 100%;min-width: 800px;">
	<div class="tit_st1" style="float: left;">
		<img src="../images/img/ico_tit.png"/>집합발전기 정보
	</div>
	<form id="rm_popup">
		<table class="tb4" style="padding-top: 10px;">
			<tr>
				<th><font color="red">*</font>중개사업자</th>
				<td colspan="3"><input id="enpr_nm" name="enpr_nm"  type="text" value="${results[0].enpr_nm}"/>
					<input id="enpr_id" name="enpr_id" type="hidden" value="${results[0].enpr_id }"/>
					<a class="btn_sm1" href="javascript:shSelectionList('selectionList','selectionListDialog','중개사업자 목록','Enpr','enpr_id','enpr_nm',{'enpr_tp':'02'});">
						<img src="../images/img/ico_magnifier_sm.png"/>
						검색
					</a>
				</td>
			</tr>
			<tr>
				<th><font color="red">*</font>집합발전기명</th>
				<td>
					<input name="set_gnr_id" type="hidden" value="${results[0].set_gnr_id}"/>
					<input name="set_gnr_nm" type="text" value="${results[0].set_gnr_nm}"/>
				</td>
				<th><font color="red">*</font>지역</th>
				<td>
					<select id="regn" name="regn">
						<c:forEach var="item" items="${regns}">
							<c:if test="${results[0].regn ne null }">
								<c:if test="${item.code_id eq results[0].regn}">
									<option value="${item.code_id }" selected="selected">${item.code_nm }</option>
								</c:if>
								<c:if test="${item.code_id ne results[0].regn}">
									<option value="${item.code_id }">${item.code_nm }</option>
								</c:if>
							</c:if>
							<c:if test="${results[0].regn eq null }">
								<c:if test="${item.code_id eq '01'}">
									<option value="${item.code_id }" selected="selected">${item.code_nm }</option>
								</c:if>
								<c:if test="${item.code_id ne '01'}">
									<option value="${item.code_id }">${item.code_nm }</option>
								</c:if>
							</c:if>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><font color="red">*</font>KPX등록번호</th>
				<td><input name="kpx_rgst_no" type="text" value="${results[0].kpx_rgst_no}"/></td>
				<th><font color="red">*</font>KPX등록명</th>
				<td><input name="kpx_rgst_nm" type="text" value="${results[0].kpx_rgst_nm}"/></td>
			</tr>
			<tr>
				<th>설비총용량(KW)</th>
				<td><label id="tot_eqpm_cpct">${results[0].tot_eqpm_cpct}</label></td>
				<th>발전원 개수</th>
				<td><label id="gnr_cnt">${results[0].gnr_cnt}</label></td>
			</tr>
		</table>
		<c:if test="${param.code eq 'modify'}">
			<input name="seq_no" type="hidden" value="${results[0].seq_no }"/>
		</c:if>
		<input name="mdt_enpr_info_id" type="hidden" value="${results[0].mdt_enpr_info_id }">
	</form>
	<div style="padding-top: 10px;">
		<div class="tit_st1" style="float: left;">
			<img src="../images/img/ico_tit.png"/>자원정보
		</div>
		<div style="float: right;">
			<a class="btn_big2" href="javascript:rs_add();">
				<img src="../images/img/ico_add.png"/>
				자원추가
			</a>
			<a class="btn_big2" href="javascript:rs_del();">
				<img src="../images/img/ico_delete.png"/>
				자원삭제
			</a>
		</div>
	</div>
	<div style="width: 100%;height: 200px;overflow-y:auto;">
		<table id="dataTable" class="tb4" style="min-width:800px; padding-top: 10px;">
			<tr>
				<th><input id="allChk" type="checkbox" /></th>
				<th>소규모 자원명</th>
				<th>자원 보유자명</th>
				<th>발전원</th>
				<th>용도</th>
				<th>설비용량(KW)</th>
				<th>등록일</th>
			</tr>
			<c:forEach var="result" items="${resourceList}" varStatus="status">
				<tr>
					<td><input type="checkbox"/></td>
					<td>${result.rsrs_nm}</td>
					<td>${result.enpr_nm}</td>
					<td>
						<c:forEach var="pwrGnrsItem" items="${pwrGnrs}">
							<c:if test="${pwrGnrsItem.code_id eq result.pwr_gnr}">
								<c:out value="${pwrGnrsItem.code_nm}"></c:out>
							</c:if>
						</c:forEach>
					</td>
					<td>
						<c:forEach var="usedItem" items="${useds}">
							<c:if test="${usedItem.code_id eq result.used}">
								<c:out value="${usedItem.code_nm}"></c:out>
							</c:if>
						</c:forEach>
					</td>
					<td>${result.eqpm_cpct}</td>
					<td>${result.crt_dt}</td>
					<td style="display: none;">${result.rsrs_id }</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<c:if test="${param.code eq 'add'}">
		<div class="form-group" style="float: right; width: 290px;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.rM.addSetOfResource();">
				등록
			</a>
		</div>
	</c:if>
	<c:if test="${param.code eq 'modify'}">
		<div class="form-group" style="float: right;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.rM.updateSetOfResource();">
				수정
			</a>
		</div>
		<div class="form-group" style="float: right;">
			<a class="btn_big4" style="float: right;" href="javascript:fnMap.rM.deleteSetOfResource();">
				삭제
			</a>
		</div>
	</c:if>
</div>
<div id="selectionListDialog" style="display:none;" >
	<div id="selectionList"></div>
</div>
package com.nc.contractManagement.controller;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.nc.contractManagement.service.ContractManagementService;

/**
 *  ETRI Distributed Resource/Mediation System for new re-generation Energy Exchange
 *
 *  Copyright ⓒ [2016] ETRI. All rights reserved.
 *
 *    This is a proprietary software of ETRI, and you may not use this file except in
 *  compliance with license agreement with ETRI. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of ETRI, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 *
 * com.nc.commonservice.controller : CommonServiceController.java
 * @author creme55
 * @since 2016. 10. 13.
 * @version 1.0
 * @see 
 * @Copyright © [2016] By ETRI. All rights reserved.
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *      수정일              수정자                  수정내용
 *  -------------        -----------       -------------------------
 *  2016. 10. 13.          creme55         최초 생성(공통 서비스 Controller)
 *
 * </pre>
 **/
@Controller
public class ContractManagementServiceController{
	private static final Logger log = LoggerFactory.getLogger(ContractManagementServiceController.class);
	@Autowired
	private ContractManagementService contractManagementService;			/* 공통 서비스 */
	/**
	 * <pre>
	 * 1. 개요 : 메인페이지 호출
	 * 2. 처리내용 : 메인페이지로 이동 처리
	 * </pre>
	 *
	 * @method Name : mainServicePage
	 * @param 
	 * @return 
	 * @throws
	 * 
	 */
	@RequestMapping(value="/contractManagement/contractManagement.do", method={RequestMethod.GET, RequestMethod.POST})
	public String contractManagementServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", contractManagementService.getDataList("contractManagement.selectContractManagementList", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "contractManagement/contractManagement";
	}
	@RequestMapping(value="/contractManagement/contractManagementAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String contractManagementAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", contractManagementService.getDataList("contractManagement.selectContractManagement", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "contractManagement/contractManagementAddNModify";
	}
	@ResponseBody
	@RequestMapping(value="/contractManagement/contractManagementAddCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap contractManagementAddCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		String paramList=cmParamChk(paramMap);
		if(paramList.equals("")){
			contractManagementService.putData("contractManagement.insertContractManagement", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/contractManagement/contractManagementModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap contractManagementModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		String paramList=cmParamChk(paramMap);
		if(paramList.equals("")){
			contractManagementService.putData("contractManagement.updateContractManagement", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/contractManagement/contractManagementDelCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int contractManagementDelCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		return contractManagementService.putData("contractManagement.delContractManagement", paramMap);
	}
	private String cmParamChk(Map<String, Object> paramMap){
		String paramList="";
		if(paramMap.get("cntr_wnts_dt") == null || paramMap.get("cntr_wnts_dt").equals(""))		paramList+=",계약희망일자 시작일";
		if(paramMap.get("cntr_wnte_dt") == null || paramMap.get("cntr_wnte_dt").equals(""))		paramList+=",계약희망일자 종료일";
		if(paramMap.get("cntr_terms") == null || paramMap.get("cntr_terms").equals(""))			paramList+=",계약기간";
		if(paramMap.get("pwr_mdt_fee") == null || paramMap.get("pwr_mdt_fee").equals(""))		paramList+=",전력중개 수수료";
		if(paramMap.get("mtnc_fee") == null || paramMap.get("mtnc_fee").equals(""))				paramList+=",유지보수 수수료";
		if(paramMap.get("rec_mdt_fee") == null || paramMap.get("rec_mdt_fee").equals(""))		paramList+=",REC중개거래 수수료";
		if(paramMap.get("kpx_pwr_fee") == null || paramMap.get("kpx_pwr_fee").equals(""))		paramList+=",KPX중개거래 수수료";
		if(paramMap.get("kpx_cert_fee") == null || paramMap.get("kpx_cert_fee").equals(""))		paramList+=",KPX인증서거래 수수료";
		if(paramMap.get("mdt_enpr_id") == null || paramMap.get("mdt_enpr_id").equals(""))		paramList+=",중개사업자명";
		if(paramMap.get("rsrs_id") == null || paramMap.get("rsrs_id").equals(""))				paramList+=",자원명";
		if(paramMap.get("enpr_id") == null || paramMap.get("enpr_id").equals(""))				paramList+=",자원보유자명";
		return paramList;
	}
	@Value(value="#{global['download_path']}")
	private String serverPath;
	
}
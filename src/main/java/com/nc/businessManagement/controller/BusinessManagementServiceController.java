package com.nc.businessManagement.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nc.businessManagement.service.BusinessManagementService;

/**
 *  ETRI Distributed Resource/Mediation System for new re-generation Energy Exchange
 *
 *  Copyright ⓒ [2016] ETRI. All rights reserved.
 *
 *    This is a proprietary software of ETRI, and you may not use this file except in
 *  compliance with license agreement with ETRI. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of ETRI, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 *
 * com.nc.commonservice.controller : CommonServiceController.java
 * @author creme55
 * @since 2016. 10. 13.
 * @version 1.0
 * @see 
 * @Copyright © [2016] By ETRI. All rights reserved.
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *      수정일              수정자                  수정내용
 *  -------------        -----------       -------------------------
 *  2016. 10. 13.          creme55         최초 생성(공통 서비스 Controller)
 *
 * </pre>
 **/
@Controller
public class BusinessManagementServiceController{
	private static final Logger log = LoggerFactory.getLogger(BusinessManagementServiceController.class);
	
	@Autowired
	private BusinessManagementService businessManagementService;			/* 공통 서비스 */

	/**
	 * <pre>
	 * 1. 개요 : 메인페이지 호출
	 * 2. 처리내용 : 메인페이지로 이동 처리
	 * </pre>
	 *
	 * @method Name : mainServicePage
	 * @param 
	 * @return 
	 * @throws
	 * 
	 */
	@RequestMapping(value="/businessManagement/intermediaryBusiness.do", method={RequestMethod.GET, RequestMethod.POST})
	public String intermediaryBusinessServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", businessManagementService.getDataList("businessManagement.selectIntermediaryList", paramMap));			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "businessManagement/intermediaryBusiness";
	}
	@RequestMapping(value="/businessManagement/resourceholders.do", method={RequestMethod.GET, RequestMethod.POST})
	public String resourceholdersServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", businessManagementService.getDataList("businessManagement.selectResourceHoldersList", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "businessManagement/resourceholders";
	}
	@RequestMapping(value="/businessManagement/intermediaryBusinessAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String intermediaryBusinessAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			paramMap.put("up_code_id", "enprTp");
			model.addAttribute("enprTpList", businessManagementService.getDataList("common.selectCode", paramMap));
			model.addAttribute("results", businessManagementService.getDataList("businessManagement.selectIntermediary", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "businessManagement/intermediaryBusinessAddNModify";
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/intermediaryBusinessAddCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap intermediaryBusinessAddCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkTelNo(paramMap);
		String paramList=ibParamChk(paramMap);
		if(paramList.equals("")){
			paramMap.put("enpr_tp", "02");
			businessManagementService.putData("businessManagement.insertEnprInfo", paramMap);
//			businessManagementService.putData("businessManagement.insertCntcSetInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/intermediaryBusinessModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap intermediaryBusinessModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkTelNo(paramMap);
		String paramList=ibParamChk(paramMap);
		if(paramList.equals("")){
			businessManagementService.putData("businessManagement.updateEnprInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/intermediaryBusinessDelCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int intermediaryBusinessDelCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		return businessManagementService.putData("businessManagement.delEnprInfo", paramMap);
	}
	@RequestMapping(value="/businessManagement/resourceAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String resourceAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			paramMap.put("up_code_id", "enprTp");
			model.addAttribute("enprTpList", businessManagementService.getDataList("common.selectCode", paramMap));
			model.addAttribute("results", businessManagementService.getDataList("businessManagement.selectResourceHolders", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "businessManagement/resourceAddNModify";
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/resourceAddCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap resourceAddCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkTelNo(paramMap);
		String paramList=ibParamChk(paramMap);
		if(paramList.equals("")){
			paramMap.put("enpr_tp", "01");
			businessManagementService.putData("businessManagement.insertEnprInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);			
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/resourceModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap resourceModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkTelNo(paramMap);
		String paramList=ibParamChk(paramMap);
		if(paramList.equals("")){
			businessManagementService.putData("businessManagement.updateEnprInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);			
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/businessManagement/resourceDelCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int resourceDelCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		return businessManagementService.putData("businessManagement.delEnprInfo", paramMap);
	}
	private void mkTelNo(Map<String, Object> paramMap){
		paramMap.put("tel_no", paramMap.get("tel_no1")+"-"+paramMap.get("tel_no2")+"-"+paramMap.get("tel_no3"));
		paramMap.put("fax_no", paramMap.get("fax_no1")+"-"+paramMap.get("fax_no2")+"-"+paramMap.get("fax_no3"));
		paramMap.put("cnct_no", paramMap.get("cnct_no1")+"-"+paramMap.get("cnct_no2")+"-"+paramMap.get("cnct_no3"));
		paramMap.put("addr", paramMap.get("enpr_addr_number")+" "+paramMap.get("enpr_addr"));		
	}
	private String ibParamChk(Map<String, Object> paramMap){
		String paramList="";
		if(paramMap.get("enpr_nm")==null || paramMap.get("enpr_nm").equals(""))					paramList += ",사업자명";
		if(paramMap.get("enpr_ceo_nm")==null || paramMap.get("enpr_ceo_nm").equals(""))			paramList += ",대표자명";
		if(paramMap.get("enpr_rgst_no")==null || paramMap.get("enpr_rgst_no").equals(""))		paramList += ",사업자 등록번호";
		if(paramMap.get("enpr_cond")==null || paramMap.get("enpr_cond").equals(""))				paramList += ",업태";
		if(paramMap.get("enpr_cnct_nm")==null || paramMap.get("enpr_cnct_nm").equals(""))		paramList += ",담당자 이름";
		if(paramMap.get("cnct_no1")==null || paramMap.get("cnct_no1").equals(""))				paramList += ",담당자 전화번호";
		else if(paramMap.get("cnct_no2")==null || paramMap.get("cnct_no2").equals(""))			paramList += ",담당자 전화번호";
		else if(paramMap.get("cnct_no3")==null || paramMap.get("cnct_no3").equals(""))			paramList += ",담당자 전화번호";
		if(paramMap.get("enpr_addr")==null || paramMap.get("enpr_addr").equals(""))				paramList += ",기본 주소";
		if(paramMap.get("enpr_dtl_addr")==null || paramMap.get("enpr_dtl_addr").equals(""))		paramList += ",상세주소";
		return paramList;
	}
	@Value(value="#{global['download_path']}")
	private String serverPath;
	
}
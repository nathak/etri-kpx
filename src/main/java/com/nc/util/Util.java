package com.nc.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String makeBidding(List<?> result,String spanRow) throws Exception {
		String jsonStr="";
		ObjectMapper om = new ObjectMapper();
		if(result.size()!=0){
			Map<?, ?> m;
			String start="";
			int startNumber=0;
			int cnt=0;
			for(int num=0;num<result.size();num++){
				m = (Map<?, ?>)result.get(num);
				if(!start.equals((String)m.get(spanRow))){
					start=(String) m.get(spanRow);
					if(num!=0){
						HashMap attr = new HashMap();	
						attr.put(spanRow, new HashMap());
						((HashMap)attr.get(spanRow)).put("rowspan", ""+cnt);
						((HashMap)result.get(startNumber)).put("attr", attr);
						cnt=0;
					}
					startNumber=num;
				}
				else{
					HashMap attr = new HashMap();	
					attr.put(spanRow, new HashMap());
					((HashMap)attr.get(spanRow)).put("display", "none");
					((HashMap)result.get(num)).put("attr", attr);
				}
				cnt++;
			}
			HashMap attr = new HashMap();	
			attr.put(spanRow, new HashMap());
			((HashMap)attr.get(spanRow)).put("rowspan", ""+cnt);
			((HashMap)result.get(startNumber)).put("attr", attr);
			jsonStr = om.writeValueAsString(result);
		}else{
			jsonStr="[]";
		}
//		System.out.println(jsonStr);
		return jsonStr;
	}
}

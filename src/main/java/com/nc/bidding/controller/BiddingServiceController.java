package com.nc.bidding.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nc.bidding.service.BiddingService;
import com.nc.util.Util;

/**
 *  ETRI Distributed Resource/Mediation System for new re-generation Energy Exchange
 *
 *  Copyright ⓒ [2016] ETRI. All rights reserved.
 *
 *    This is a proprietary software of ETRI, and you may not use this file except in
 *  compliance with license agreement with ETRI. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of ETRI, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 *
 * com.nc.commonservice.controller : CommonServiceController.java
 * @author creme55
 * @since 2016. 10. 13.
 * @version 1.0
 * @see 
 * @Copyright © [2016] By ETRI. All rights reserved.
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *      수정일              수정자                  수정내용
 *  -------------        -----------       -------------------------
 *  2016. 10. 13.          creme55         최초 생성(공통 서비스 Controller)
 *
 * </pre>
 **/
@Controller
public class BiddingServiceController{
	private static final Logger log = LoggerFactory.getLogger(BiddingServiceController.class);
	
	@Autowired
	private BiddingService biddingService;			/* 공통 서비스 */

	/**
	 * <pre>
	 * 1. 개요 : 메인페이지 호출
	 * 2. 처리내용 : 메인페이지로 이동 처리
	 * </pre>
	 *
	 * @method Name : mainServicePage
	 * @param 
	 * @return 
	 * @throws
	 * 
	 */
	@RequestMapping(value="/bidding/powerBid.do", method={RequestMethod.GET, RequestMethod.POST})
	public String powerBidServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		return "bidding/powerBid";
	}
	@RequestMapping(value="/bidding/recBid.do", method={RequestMethod.GET, RequestMethod.POST})
	public String recBidServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("iBNmList", biddingService.getDataList("common.selectMdtNmList", paramMap));			
			model.addAttribute("results", Util.makeBidding(biddingService.getDataList("bidding.selectRecBid", paramMap),"sle_wnt_dt"));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "bidding/recBid";
	}
	@RequestMapping(value="/bidding/powerBidSub1.do", method={RequestMethod.GET, RequestMethod.POST})
	public String powerBidSub1ServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", Util.makeBidding(biddingService.getDataList("bidding.selectBidSub1", paramMap),"sle_wnt_dt"));
			model.addAttribute("iBNmList", biddingService.getDataList("common.selectMdtNmList", paramMap));			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "bidding/powerBidSub1";
	}
	@RequestMapping(value="/bidding/powerBidSub2.do", method={RequestMethod.GET, RequestMethod.POST})
	public String powerBidSub2ServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", Util.makeBidding(biddingService.getDataList("bidding.selectBidSub2", paramMap),"sle_wnt_dt"));
			model.addAttribute("iBNmList", biddingService.getDataList("common.selectMdtNmList", paramMap));			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "bidding/powerBidSub2";
	}
	@RequestMapping(value="/bidding/powerBidSub1.json", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap powerBidSub1JsonServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		ModelMap result = new ModelMap();
		result.put("results", Util.makeBidding(biddingService.getDataList("bidding.selectBidSub1", paramMap),"sle_wnt_dt"));
		return result;
	}
	@RequestMapping(value="/bidding/powerBidSub2.json", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap powerBidSub2JsonServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		ModelMap result = new ModelMap();
		result.put("results", Util.makeBidding(biddingService.getDataList("bidding.selectBidSub2", paramMap),"sle_wnt_dt"));
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/bidding/bidModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int bidModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ObjectMapper om = new ObjectMapper();
	    TypeReference<List<Integer>> typeRef = new TypeReference<List<Integer>>() {};
		List<Integer> list = (List<Integer>)om.readValue((String)paramMap.get("data"), typeRef);
		paramMap.put("seqs", list.toArray());
		paramMap.put("stcc", paramMap.get("code").equals("0")?"03":"04");
		biddingService.putData("bidding.updateElec", paramMap);
//		System.out.println("==============================");
//		System.out.println(paramMap.get("data"));
//		System.out.println(paramMap.get("code").equals("0")?"03":"04");
//		System.out.println("==============================");
		return 0;
	}	
	@RequestMapping(value="/bidding/recBid.json", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap recBidJsonServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		ModelMap result = new ModelMap();
		result.put("results", Util.makeBidding(biddingService.getDataList("bidding.selectRecBid", paramMap),"sle_wnt_dt"));
		return result;
	}
	@RequestMapping(value="/bidding/powerBidAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String powerBidAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		
		return "bidding/powerBidAddNModify";
	}
	@RequestMapping(value="/bidding/recBidAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String recBidAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		
		return "bidding/recBidAddNModify";
	}
	
	@Value(value="#{global['download_path']}")
	private String serverPath;
	
}
package com.nc.resourceManagement.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nc.commonservice.service.CommonService;
import com.nc.resourceManagement.service.ResourceManagementService;
@Controller
public class ResourceManagementServiceController{
	private static final Logger log = LoggerFactory.getLogger(ResourceManagementServiceController.class);
	@Autowired
	private ResourceManagementService resourceManagementService;			/* 공통 서비스 */
	/**
	 * <pre>
	 * 1. 개요 : 메인페이지 호출
	 * 2. 처리내용 : 메인페이지로 이동 처리
	 * </pre>
	 *
	 * @method Name : mainServicePage
	 * @param 
	 * @return 
	 * @throws
	 * 
	 */
	@RequestMapping(value="/resourceManagement/setOfResource.do", method={RequestMethod.GET, RequestMethod.POST})
	public String setOfResourceServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", resourceManagementService.getDataList("resourceManagement.selectSetOfResourceList", paramMap));
			model.addAttribute("nameList", resourceManagementService.getDataList("common.selectMdtNmList", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "resourceManagement/setOfResource";
	}
	@RequestMapping(value="/resourceManagement/smallResource.do", method={RequestMethod.GET, RequestMethod.POST})
	public String smallResourceServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			paramMap.put("up_code_id", "REGN");
			model.addAttribute("regns", resourceManagementService.getDataList("common.selectCode", paramMap));
			model.addAttribute("results", resourceManagementService.getDataList("resourceManagement.selectSmallResourceList", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "resourceManagement/smallResource";
	}
	@RequestMapping(value="/resourceManagement/setOfResourceAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String setOfResourceAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		List<?> result;
		ObjectMapper om = new ObjectMapper();
		try {
			result = resourceManagementService.getDataList("resourceManagement.selectSetOfResource", paramMap);
			paramMap.put("up_code_id", "REGN");
			model.addAttribute("regns", resourceManagementService.getDataList("common.selectCode", paramMap));
			paramMap.put("up_code_id", "USED");
			model.addAttribute("useds", resourceManagementService.getDataList("common.selectCode", paramMap));
			paramMap.put("up_code_id", "RSRS_DV_CD");
			model.addAttribute("pwrGnrs", resourceManagementService.getDataList("common.selectCode", paramMap));
			model.addAttribute("results", result);
			model.addAttribute("resourceList", resourceManagementService.getDataList("resourceManagement.selectResourceList", paramMap));
			if(result.size()!=0)
				paramMap.put("regn",((Map<String, Object>)result.get(0)).get("regn"));
			model.addAttribute("usedsJson", om.writeValueAsString(model.get("useds")));
			model.addAttribute("pwrGnrsJson", om.writeValueAsString(model.get("pwrGnrs")));
			model.addAttribute("resourceAllList", om.writeValueAsString(resourceManagementService.getDataList("resourceManagement.selectAllResourceList", paramMap)));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "resourceManagement/setOfResourceAddNModify";
	}
	@RequestMapping(value="/resourceManagement/smallResourceAddNModify.do", method={RequestMethod.GET, RequestMethod.POST})
	public String smallResourceAddNModifyServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			model.addAttribute("results", resourceManagementService.getDataList("resourceManagement.selectSmallResource", paramMap));
			paramMap.put("up_code_id", "USED");
			model.addAttribute("useds", resourceManagementService.getDataList("common.selectCode", paramMap));		
			paramMap.put("up_code_id", "REGN");
			model.addAttribute("regns", resourceManagementService.getDataList("common.selectCode", paramMap));	
			paramMap.put("up_code_id", "RSRS_DV_CD");
			model.addAttribute("pwrGnrs", resourceManagementService.getDataList("common.selectCode", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "resourceManagement/smallResourceAddNModify";
	}
	@RequestMapping(value="/resourceManagement/setOfResourceData.do", method={RequestMethod.GET, RequestMethod.POST})
	public String setOfResourceDataServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, ModelMap model) throws Exception {
		try {
			paramMap.put("up_code_id", "REGN");
			model.addAttribute("regns", resourceManagementService.getDataList("common.selectCode", paramMap));
			model.addAttribute("results", resourceManagementService.getDataList("resourceManagement.selectSetOfResourceList", paramMap));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "resourceManagement/setOfResourceData";
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/setOfResourceModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap setOfResourceModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		String paramList=setOfParamChk(paramMap);
		if(paramList.equals("")){
			resourceManagementService.putData("resourceManagement.updateSetOfResource", paramMap);
			if(paramMap.get("rsIdList")!=null){
				paramMap.put("rsIdList", paramMap.get("rsIdList").toString().split(","));
				resourceManagementService.putData("resourceManagement.updateSetOfResourceSub1", paramMap);
				resourceManagementService.putData("resourceManagement.updateSetOfResourceSub2", paramMap);
				resourceManagementService.putData("resourceManagement.updateSetOfResourceSub3", paramMap);
			}
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/setOfResourceAddCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap setOfResourceAddCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		String paramList=setOfParamChk(paramMap);
		if(paramList.equals("")){
			paramMap.putAll((Map<String, Object>)resourceManagementService.getDataList("resourceManagement.getMdtIds", paramMap).get(0));
			resourceManagementService.putData("resourceManagement.insertSetOfResource", paramMap);
			resourceManagementService.putData("resourceManagement.insertCntcSetInfo", paramMap);
			if(paramMap.get("rsIdList")!=null){
				paramMap.put("rsIdList", paramMap.get("rsIdList").toString().split(","));
				resourceManagementService.putData("resourceManagement.updateSetOfResourceSub2", paramMap);
			}
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/smallResourceAddCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap resourceAddCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkSmallAddr(paramMap);
		String paramList=smallParamChk(paramMap);
		if(paramList.equals("")){
			resourceManagementService.putData("resourceManagement.insertCntcRsrsInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/smallResourceModCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap resourceModCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		mkSmallAddr(paramMap);
		String paramList=smallParamChk(paramMap);
		if(paramList.equals("")){
			resourceManagementService.putData("resourceManagement.updateCntcRsrsInfo", paramMap);
			result.put("status", 0);
		}else{
			result.put("status", 1);
			result.put("params", paramList);
		}	
		/* 
		 * 서명파일이나 첨부파일에 대한 아이디 생성을 위한 작업 
		 * HttpServletRequest객체에 serviceType 추가
		 */
		request.setAttribute("serviceType", "smallResource");
		
		return result;
	}
	private String setOfParamChk(Map<String, Object> paramMap){
		String paramList="";
		if(paramMap.get("set_gnr_nm")==null || paramMap.get("set_gnr_nm").equals(""))		paramList += ",집합 발전기명";
		if(paramMap.get("kpx_rgst_nm")==null || paramMap.get("kpx_rgst_nm").equals(""))		paramList += ",kpx등록명";
		if(paramMap.get("kpx_rgst_no")==null || paramMap.get("kpx_rgst_no").equals(""))		paramList += ",kpx등록 번호";
//		if(paramMap.get("set_gnr_id")==null || paramMap.get("set_gnr_id").equals(""))		paramList += ",집합 발전기 아이디";
//		if(paramMap.get("mdt_enpr_id")==null || paramMap.get("mdt_enpr_id").equals(""))		paramList += ",중개사업자 이름";
		if(paramMap.get("regn")==null || paramMap.get("regn").equals(""))					paramList += ",지역";
		return paramList;
	}
	@RequestMapping(value="/resourceManagement/getRessource.json", method={RequestMethod.GET, RequestMethod.POST})
	public ModelMap getRsrsServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		ModelMap result = new ModelMap();
		ObjectMapper om = new ObjectMapper();
		result.put("results", om.writeValueAsString(resourceManagementService.getDataList("resourceManagement.selectAllResourceList", paramMap)));
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/setOfResourceDelCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int setOfResourceDelCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		int result = resourceManagementService.putData("resourceManagement.delSetOfResource", paramMap);
		if(paramMap.get("rsIdList")!=null){
			paramMap.put("rsIdList", paramMap.get("rsIdList").toString().split(","));
			result = resourceManagementService.putData("resourceManagement.updateSetOfResourceSub1", paramMap);
			result = resourceManagementService.putData("resourceManagement.delSetOfResourceSub3", paramMap);
		}
		return result;
	}
	@ResponseBody
	@RequestMapping(value="/resourceManagement/smallResourceDelCmd.do", method={RequestMethod.GET, RequestMethod.POST})
	public int resourceDelCmdServicePage(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
		return resourceManagementService.putData("resourceManagement.delCntcRsrsInfo", paramMap);
	}
	private void mkSmallAddr(Map<String, Object> paramMap){
		paramMap.put("addr", paramMap.get("rsrs_addr_number")+" "+paramMap.get("rsrs_bas_addr"));
	}
	private String smallParamChk(Map<String, Object> paramMap){
		String paramList="";
		if(paramMap.get("rsrs_nm")==null || paramMap.get("rsrs_nm").equals(""))					paramList += ",자원명";
		if(paramMap.get("addr")==null || paramMap.get("addr").equals(""))						paramList += ",주소지(기본)";
		if(paramMap.get("rsrs_dtl_addr")==null || paramMap.get("rsrs_dtl_addr").equals(""))		paramList += ",주소지(상세)";
		if(paramMap.get("regn")==null || paramMap.get("regn").equals(""))						paramList += ",지역";
		if(paramMap.get("used")==null || paramMap.get("used").equals(""))						paramList += ",용도";
		if(paramMap.get("eqpm_cpct")==null || paramMap.get("eqpm_cpct").equals(""))				paramList += ",발전용량";
		if(paramMap.get("pwr_gnr")==null || paramMap.get("pwr_gnr").equals(""))			paramList += ",발전원";
		if(paramMap.get("instl_lnd")==null || paramMap.get("instl_lnd").equals(""))		paramList += ",설치부지(지목)";
		if(paramMap.get("instl_own")==null || paramMap.get("instl_own").equals(""))				paramList += ",설치부지(소유자)";
		if(paramMap.get("instl_use")==null || paramMap.get("instl_use").equals(""))				paramList += ",설치부지(사용권원)";
		if(paramMap.get("lcns_no")==null || paramMap.get("lcns_no").equals(""))					paramList += ",허가번호";
		if(paramMap.get("rgst_no")==null || paramMap.get("rgst_no").equals(""))					paramList += ",등록번호";
		if(paramMap.get("wtt_hrmtr_no")==null || paramMap.get("wtt_hrmtr_no").equals(""))		paramList += ",전력량계번호";
		if(paramMap.get("max_gnr_cpct")==null || paramMap.get("max_gnr_cpct").equals(""))		paramList += ",최대발전용량";
		if(paramMap.get("mix_gnr_cpct")==null || paramMap.get("mix_gnr_cpct").equals(""))		paramList += ",최소발전용량";
		if(paramMap.get("ctrl_rtu_prt")==null || paramMap.get("ctrl_rtu_prt").equals(""))		paramList += ",관제용 RTU PORT";
		if(paramMap.get("ctrl_rtu_ip")==null || paramMap.get("ctrl_rtu_ip").equals(""))		paramList += ",관제용 RTU IP";
		if(paramMap.get("eqpm_cnt")==null || paramMap.get("eqpm_cnt").equals(""))				paramList += ",설비대수";
		if(paramMap.get("work_st_dt")==null || paramMap.get("work_st_dt").equals(""))			paramList += ",운전시작일";
		if(paramMap.get("enpr_id")==null || paramMap.get("enpr_id").equals(""))			paramList += ",운전시작일";
		return paramList;
	}
	@Value(value="#{global['download_path']}")
	private String serverPath;
	
}